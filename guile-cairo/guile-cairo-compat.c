/* guile-cairo
 * Copyright (C) 2007, 2011, 2020 Andy Wingo <wingo at pobox dot com>
 *
 * guile-cairo-compat.c: Guile 1.8 support
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *                                                                  
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *                                                                  
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <libguile.h>

#include "guile-cairo-compat.h"


#if SCM_NEEDS_COMPAT (3, 0, 0)

/* If backwards-compatibility code is needed, add it here.  */

#endif /* NEEDS_COMPAT (3.0) */
